/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per term_inarlo.
"Replace Shred" serve a far ripartire il programma.
----------------------------------------------------*/

// RTcMix Lookup table: https://en.wikipedia.org/wiki/Real-time_Cmix
Gen10 oscils[13];
Phasor phasor;              // utilizzato per controllare le look-up table contentute in 'oscils'

0 => int selected_oscil;    // oscillatore selezionato (0. sine, 1. square, 2. tri, 3. sawtooth)
-1 => int previous_oscil;   // variabile utilizzata nel ciclo while
["SINE", "SQUARE", "TRIANGLE", "SAWTOOTH"] @=> string waveform_name[];

// FILTRI
LPF lowpass;
BPF bandpass;
HPF highpass;
Step _c => Envelope cutoff_interp => blackhole;
_c.next(1);

0.5 => float raw_cutoff;    // valore assoluto del cutoff - range 0-1
float cutoff;
float Q;
set_cutoff( raw_cutoff );
set_Q(0);
0 => int filter_id; // id del filtro selezionato - utilizzato per connettere gli oscillatori solo al filtro in uso
0 => int previous_filter_id;
// lancia 'cutoff_update()' su uno 'shread' (thread) parallelo
spork ~ cutoff_update();
//

Step step => Envelope line => phasor;
// usa il low-pass filter di default
Envelope env => lowpass => Gain master => dac;
// interpola l'amplitude del synth per evitare glitch - miglioramento rispetto alla soluzione proposta fino al tutorial nr 8
Step m_step => Envelope m_env => master;
master.op(3);   // i segnali inviati a 'master' vengono moltiplicati tra di loro
m_env.time( 0.005 ); // 5 msec
m_env.target(0);

step.next(1);               // imposta il valore di 'step' su 1
phasor.sync(0);

init_oscils();              // inizializza gli UGen Gen10

// connessioni MIDI
MidiIn m_in;                // crea un input MIDI
MidiMsg msg_in;             // crea un contenitore per i messaggi MIDI ricevuti
/*
    seleziona la porta MIDI che corrisponde al
    controller in uso (CTRL+2 per controllare le porte MIDI)
*/
"Launchkey MK2 25 MIDI 1" => string device_name;
m_in.open( device_name );

// variabili
144 => int NOTE_ON;
128 => int NOTE_OFF;
176 => int CTRL_CHANGE;
0   => int MIDI_CHANNEL;  // canale MIDI - range da 0 a 15

float fr;
float amp;
int midi_note;
int last_note;
0.007874016 => float divided_by_127;
0.05        => float freq_interpolation;    // in secondi

[21,22,23,24,25,26,27,28] @=> int knobs[];  // control change corrispondenti agli 8 potenziometri presenti sul controller

0.5 => amp;
master.gain( amp );                  // impostare l'ampiezza del Gain 'master'
15::ms  => dur attack_time;          // tempo di attacco
300::ms => dur release_time;         // tempo di rilascio

line.time( freq_interpolation );     // imposta il tempo di interpolazione della frequenza dell'oscillatore

<<< "Basic Mono Synth with Sine, Square, Triangle and Sawtooth oscillators" >>>;
<<< "waveform: " + waveform_name[ selected_oscil ] >>>;

// loop infinito
while( true )
{
    // avanza ad ogni messaggio MIDI ricevuto
    m_in => now;

    while( m_in.recv( msg_in ) )
    {
        if( msg_in.data1 == ( NOTE_ON + MIDI_CHANNEL ) && msg_in.data3 != 0 )
        {
            msg_in.data2 => midi_note;
            set_oscils( midi_note );        // seleziona la wavetable in base alla nota ricevuta e l'oscillatore selezionato
            Std.mtof( midi_note ) => fr;    // conversione da MIDI a Freq in Hz
            line.target( fr );              // assegnare la frequenza a 'line' che controlla la freq dell'oscillatore
            msg_in.data2 => last_note;
            set_cutoff( raw_cutoff );
            msg_in.data3 * divided_by_127 => float amplitude;
            m_env.target( amplitude );
            env.duration( attack_time );
            env.keyOn(1);
        }

        // controlla che il messaggio MIDI sia un NOTE OFF o un NOTE ON con velocity 0
        // in entrambi i casi la nota MIDI deve combaciare con l'ultima suonata (playedNote)
        else if( ( msg_in.data1 == ( NOTE_ON + MIDI_CHANNEL ) && msg_in.data3 == 0 && msg_in.data2 == last_note )
                  || ( msg_in.data1 == ( NOTE_OFF + MIDI_CHANNEL ) && msg_in.data2 == last_note ) )
        {
            env.duration( release_time );
            env.keyOff(1);
        }

        // portamento
        else if( msg_in.data1 == ( CTRL_CHANGE+MIDI_CHANNEL ) && msg_in.data2 == knobs[0] )
        {
            set_interp( msg_in.data3 ) => freq_interpolation;
            line.time( freq_interpolation );
        }

        // seleziona la forma d'onda
        else if( msg_in.data1 == ( CTRL_CHANGE+MIDI_CHANNEL ) && msg_in.data2 == knobs[1] )
        {
            msg_in.data3 * 4 => int x;
            x / 128 => selected_oscil;
            if( selected_oscil != previous_oscil )
            {
                set_oscils( midi_note );                                // seleziona la wavetable in base alla nota ricevuta e l'oscillatore selezionato
                <<< "waveform: " + waveform_name[ selected_oscil ] >>>;
                selected_oscil => previous_oscil;
            }
        }

        // seleziona il filtro
        else if( msg_in.data1 == ( CTRL_CHANGE+MIDI_CHANNEL ) && msg_in.data2 == knobs[2] )
        {
            ( ( msg_in.data3 / 128. ) * 3 ) $ int => filter_id;
            // chiama 'select_filter()' solo quando 'filter_id' cambia valore
            if( filter_id != previous_filter_id )
            {
                select_filter( filter_id );
                filter_id => previous_filter_id;
            }
        }

        // cutoff
        else if( msg_in.data1 == ( CTRL_CHANGE+MIDI_CHANNEL ) && msg_in.data2 == knobs[3] )
        {
            msg_in.data3 * divided_by_127 => raw_cutoff;
            set_cutoff( raw_cutoff );
            <<< "CUTOFF: " + cutoff >>>;
        }

        // Q - resonance
        else if( msg_in.data1 == ( CTRL_CHANGE+MIDI_CHANNEL ) && msg_in.data2 == knobs[4] )
        {
            set_Q( msg_in.data3 * divided_by_127 );
            <<< "filter Q: " + Q >>>;
        }

    }
}

// -------------FUNZIONI--------------
function float set_interp( float x )
{
    x * divided_by_127 => x;        // scala in un range da 0 a 1
    x * x * x => x;                 // rendi la funzione esponenziale
    ( x * 0.997 ) + 0.001 => x;     // scala in un range da 1 a 1000 millisecondi

    <<< "Freq Interpolation: " + x + " sec" >>>;

    return x;
}

function void set_oscils( int x )
{
    /*
        Seleziona l'UGen Gen10 in base alla nota ricevuta.
        I diversi Gen10 hanno un diverso numero di armoniche,
        piu' e' alta la nota selezionata meno armoniche si avranno
        per evitare problemi di aliasing
    */

    0 => int pointer;     // utilizzato come puntatore per l'array 'oscils'

    // quando l'oscillatore selezionato NON e' sinusoidale
    if( selected_oscil > 0 )
    {
        ( ( selected_oscil - 1 ) * 4 ) + 1 => pointer;

        if( x <= 70 && oscils[ pointer ].isConnectedTo( env ) == 0 )
        {
            for( 0 => int c; c < oscils.size(); c++ )
            {
                if( c == pointer )
                {
                    phasor => oscils[c] => env;
                }
                else
                {
                    phasor =< oscils[c] =< env;
                }
            }
        }

        else if(  x > 70 && x <= 90 && oscils[ pointer + 1 ].isConnectedTo( env ) == 0 )
        {
            for( 0 => int c; c < oscils.size(); c++ )
            {
                if( c == ( pointer + 1 ) )
                {
                    phasor => oscils[c] => env;
                }
                else
                {
                    phasor =< oscils[c] =< env;
                }
            }
        }

        else if( x > 90 && x <= 108 && oscils[ pointer + 2 ].isConnectedTo( env ) == 0 )
        {
            for( 0 => int c; c < oscils.size(); c++ )
            {
                if( c == ( pointer + 2 ) )
                {
                    phasor => oscils[c] => env;
                }
                else
                {
                    phasor =< oscils[c] =< env;
                }
            }
        }

        else if( x > 108 && x <= 127 && oscils[ pointer + 3 ].isConnectedTo( env ) == 0 )
        {
            for( 0 => int c; c < oscils.size(); c++ )
            {
                if( c == ( pointer + 3 ) )
                {
                    phasor => oscils[c] => env;
                }
                else
                {
                    phasor =< oscils[c] =< env;
                }
            }
        }
    }

    // quando l'oscillatore selezionato e' sinusoidale
    else if( selected_oscil == 0 )
    {
        if( oscils[ pointer ].isConnectedTo( env ) == 0 )
        {
            for( 0 => int c; c < oscils.size(); c++ )
            {
                if( c == pointer )
                {
                    phasor => oscils[c] => env;
                }
                else
                {
                    phasor =< oscils[c] =< env;
                }
            }
        }
    }
}

function void init_oscils()
{
    /*
        Inizializza l'array di Gen10 'oscils'.
        Il primo elemento dell'array e' una sinusoide, a seguire si ha
        onda quadra, triangolare e dente di sega, le quali utilizzano
        4 Gen10 ognuna.
    */

    float coefficients[0];            // coefficienti degli UGen Gen10

    // onda sinusoidale - oscils[0]
    coefficients.size(1);
    1 => coefficients[0];
    oscils[0].coefs( coefficients );

    // onda quadra - formula: y = sum[(1/k)*sin(2PI*f*k*t)]; con k=1,3,5,7,9,...
    // quadra: 14 parziali - oscils[1]
    for( 1 => int c; c < 15; c++ )
    {
        ( ( c * 2 ) - 1 ) => int odd; // solo numeri dispari
        coefficients.size( odd );     // ridimensiona l'array
        1. / odd => float amp;        // 1/n
        amp => coefficients[ odd-1 ];
    }
    oscils[1].coefs( coefficients );

    // quadra: 7 parziali - oscils[2]
    for( 1 => int c; c < 8; c++ )
    {
        ( ( c * 2 ) - 1 ) => int odd;
        coefficients.size( odd );
        1. / odd => float amp;
        amp => coefficients[ odd-1 ];
    }
    oscils[2].coefs( coefficients );

    // quadra: 3 parziali - oscils[3]
    for( 1 => int c; c < 3; c++ )
    {
        ( ( c * 2 ) - 1 ) => int odd;
        coefficients.size( odd );
        1. / odd => float amp;
        amp => coefficients[ odd-1 ];
    }
    oscils[3].coefs( coefficients );

    // quadra: 1 parziale - sinusoide - oscils[4]
    for( 1 => int c; c < 2; c++ )
    {
        ( ( c * 2 ) - 1 ) => int odd;
        coefficients.size( odd );
        1. / odd => float amp;
        amp => coefficients[ odd-1 ];
    }
    oscils[4].coefs( coefficients );

    // onda triangolare - formula: y = sum[(1/k^2)*sin(2PI*f*k*t + theta)]; con k=1,3,5,7,9,... e theta=0,180,0,180 in gradi
    // triangolare: 14 parziali - oscils[5]
    for( 1 => int c; c < 15; c++ )
    {
        float theta;
        /*
            theta=0 quando c e' dispari, theta=180 quando c e' pari.
            La fase viene espressa attraverso l'ampiezza. Ampiezza positiva quando theta=0,
            ampiezza negativa quando theta=180, cioe' si ha la fase inversa.
        */
        if( ( c % 2 ) == 1 )
        {
            1 => theta;
        }
        else
        {
            -1 => theta;
        }
        ( ( c * 2 ) - 1 ) => int odd;                         // solo numeri dispari
        coefficients.size( odd );                             // ridimensiona l'array
        theta * ( 1. / ( odd * odd ) ) => float amp;          // 1/(n^2) * theta
        amp => coefficients[ odd-1 ];
    }
    oscils[5].coefs( coefficients );

    // triangolare: 7 parziali - oscils[6]
    for( 1 => int c; c < 8; c++ )
    {
        float theta;
        /*
            theta=0 quando c e' dispari, theta=180 quando c e' pari.
            La fase viene espressa attraverso l'ampiezza. Ampiezza positiva quando theta=0,
            ampiezza negativa quando theta=180, cioe' si ha la fase inversa.
        */
        if( ( c % 2 ) == 1 )
        {
            1 => theta;
        }
        else
        {
            -1 => theta;
        }
        ( ( c * 2 ) - 1 ) => int odd;                         // solo numeri dispari
        coefficients.size( odd );                             // ridimensiona l'array
        theta * ( 1. / ( odd * odd ) ) => float amp;          // 1/(n^2) * theta
        amp => coefficients[ odd-1 ];
    }
    oscils[6].coefs( coefficients );

    // triangolare: 3 parziali - oscils[7]
    for( 1 => int c; c < 4; c++ )
    {
        float theta;
        /*
            theta=0 quando c e' dispari, theta=180 quando c e' pari.
            La fase viene espressa attraverso l'ampiezza. Ampiezza positiva quando theta=0,
            ampiezza negativa quando theta=180, cioe' si ha la fase inversa.
        */
        if( ( c % 2 ) == 1 )
        {
            1 => theta;
        }
        else
        {
            -1 => theta;
        }
        ( ( c * 2 ) - 1 ) => int odd;                         // solo numeri dispari
        coefficients.size( odd );                             // ridimensiona l'array
        theta * ( 1. / ( odd * odd ) ) => float amp;          // 1/(n^2) * theta
        amp => coefficients[ odd-1 ];
    }
    oscils[7].coefs( coefficients );

    // triangolare: 1 parziale - sinusoide - oscils[8]
    for( 1 => int c; c < 2; c++ )
    {
        float theta;
        /*
            theta=0 quando c e' dispari, theta=180 quando c e' pari.
            La fase viene espressa attraverso l'ampiezza. Ampiezza positiva quando theta=0,
            ampiezza negativa quando theta=180, cioe' si ha la fase inversa.
        */
        if( ( c % 2 ) == 1 )
        {
            1 => theta;
        }
        else
        {
            -1 => theta;
        }
        ( ( c * 2 ) - 1 ) => int odd;                         // solo numeri dispari
        coefficients.size( odd );                             // ridimensiona l'array
        theta * ( 1. / ( odd * odd ) ) => float amp;          // 1/(n^2) * theta
        amp => coefficients[ odd-1 ];
    }
    oscils[8].coefs( coefficients );

    // onda a dente di sega - formula: y = sum[(1/k)*sin(2PI*f*k*t)]; con k=1,2,3,4,5,...
    // dente di sega: 14 parziali - oscils[9]
    for( 1 => int c; c < 15; c++ )
    {
        coefficients.size( c );       // ridimensiona l'array
        ( 1. / c ) => float amp;      // 1/n
        amp => coefficients[ c-1 ];
    }
    oscils[9].coefs( coefficients );

    // dente di sega: 7 parziali - oscils[10]
    for( 1 => int c; c < 8; c++ )
    {
        coefficients.size( c );       // ridimensiona l'array
        ( 1. / c ) => float amp;      // 1/n
        amp => coefficients[ c-1 ];
    }
    oscils[10].coefs( coefficients );

    // dente di sega: 3 parziali - oscils[11]
    for( 1 => int c; c < 4; c++ )
    {
        coefficients.size( c );      // ridimensiona l'array
        ( 1. / c ) => float amp;     // 1/n
        amp => coefficients[ c-1 ];
    }
    oscils[11].coefs( coefficients );

    // dente di sega: 1 parziale - sinusoide - oscils[12]
    for( 1 => int c; c < 2; c++ )
    {
        coefficients.size( c );      // ridimensiona l'array
        ( 1. / c ) => float amp;     // 1/n
        amp => coefficients[ c-1 ];
    }
    oscils[12].coefs( coefficients );
}

function void cutoff_update()
{
    /*
        questa funzione viene lanciata su un thread parallelo (spork)
        utilizzata per interpolare il cutoff del filtro in modo da evitare 'glitch'
    */
    float _f;
    while( true )
    {
        cutoff_interp.last() => _f;
        // al posto di 'if( _f != cutoff )' - dato che sono due numeri float!!!!!
        // aggiorna il 'cutoff' solo quando e' necessario
        if( Math.fabs( _f - cutoff ) > 0.001 )
        {
            lowpass.freq( _f );
            bandpass.freq( _f );
            highpass.freq( _f );
        }
        5::samp => now;
    }
}

function void set_cutoff( float f )
{
    // imposta il valore minimo di cutoff in base all'ultima nota suonata
    Std.mtof( midi_note ) => float min_freq;
    Math.max( 50, min_freq - ( min_freq * 0.5 ) ) => min_freq;
    Math.min( min_freq, 11950 ) => min_freq;
    ( f * f * f * ( 12000 - min_freq ) ) + min_freq => cutoff;
    // aggiorna l'UGen Envelope utilizzato per impostare il cutoff del filtro in 'modo interpolato' - vedi 'cutoff_update()'
    cutoff_interp.time( 0.01 );    // tempo di interpolazione in secondi
    cutoff_interp.target( cutoff );
}

function void set_Q( float q )
{
    ( q * 4 ) + 1 => Q;
    lowpass.Q(Q);
    bandpass.Q(Q);
    highpass.Q(Q);
}

function void select_filter( int id )
{
    /*
        sceglie tra passa basso, passa banda e passa alto
    */
    if( id == 0 )
    {
        // disconnete 'env' da tutti i filtri e tutti i filtri da 'master'
        env =< lowpass =< master;
        env =< bandpass =< master;
        env =< highpass =< master;
        // connette 'env' al filtro giusto ed il filtro giusto a 'master'
        env => lowpass => master;

        <<< "FILTRO LOWPASS" >>>;
    }
    else if( id == 1 )
    {
        // disconnete 'env' da tutti i filtri e tutti i filtri da 'master'
        env =< lowpass =< master;
        env =< bandpass =< master;
        env =< highpass =< master;
        // connette 'env' al filtro giusto ed il filtro giusto a 'master'
        env => bandpass => master;

        <<< "FILTRO BANDPASS" >>>;
    }
    else
    {
        // disconnete 'env' da tutti i filtri e tutti i filtri da 'master'
        env =< lowpass =< master;
        env =< bandpass =< master;
        env =< highpass =< master;
        // connette 'env' al filtro giusto ed il filtro giusto a 'master'
        env => highpass => master;

        <<< "FILTRO HIGHPASS" >>>;
    }
}
// _FUNZIONI
