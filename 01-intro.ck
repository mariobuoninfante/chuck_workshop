/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo
----------------------------------------------------*/

//oscillatore sinusoidale collegato ad un digital-to-analog converter
//cioe' la scheda audio del computer

//UGen + nome dell'ogetto => scheda audio
//UGen significa Unit Generator e sono gli "blocchi" utilizzati 
//per generare e manipolare il suono (signal processing)
SinOsc sine => dac;

//per cambiare parametri all'oscillatore
//si utilizza il suo nome (in questo caso "sine")
//e degli appositi "metodi" (ie .freq(), .gain(), etc.)
sine.freq(500); //settare la frequenza dell'oscillatore su 500Hz
sine.gain(0.3); //settare l'ampiezza dell'osc su 0.3 (range digitale: 0-1)

//loop infinito
//"while" (affinche') "true" e' sempre vero (chiaramente lo sara' per sempre)
//esegui il codice tra parentesi graffe {}
while(true)
{
    //stampa sulla console
    //per vedere la console: "CTRL+0"
    <<< "I'M RUNNING" >>>;
    
    //aspetta 1 secondo e poi continua l'esecuzione del programma (che e' un loop infinito)
    //"now" e' una parola chiave in ChucK, utilizzata per la gestione del tempo
    1::second => now;
}

//in ChucK tutto cio' che segue (senza andare a capo) i simboli "//" (senza virgolette)
//viene considerato un commento e quindi non ritenuto parte del codice.
//Un'alternativa utile nel caso di commenti lunghi piu' di un rigo (cioe' 
//si e' andati a capo piu' di una volta (premendo "enter"), e quella illustrata di seguito

/*
commenti multi rigo contenuti fra i simboli "asterisco" (moltiplicazione) seguito "slash"
all'inizio e "slash" seguito da "asterisco" alla fine
*/
