/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo.
"Replace Shred" serve a far ripartire il programma.
----------------------------------------------------*/

//oscillatore sinusoidale collegato ad un inviluppo
Step step => Envelope line => SinOsc osc => Envelope env => dac;

step.next(1); // imposta il valore di 'step' su 1
osc.sync(0);  // imposta 'osc' in modo che usi il segnale in ingresso per controllare la frequenza in Hz

//connessioni MIDI
MidiIn mIn; // crea un input MIDI
MidiMsg msgIn;  // crea un contenitore per i messaggi MIDI ricevuti

mIn.open("Launchkey MK2 25 MIDI 1");   // selezionare la porta MIDI che corrisponde al controller in uso (CTRL+2 per controllare le porte MIDI)

// variabili
144 => int NOTE_ON;
128 => int NOTE_OFF;
176 => int CTRL_CHANGE;
0 => int MIDI_CHANNEL;  // MIDI channel da 0 a 15

float fr;
float amp;
int midiNote;
int lastNote;
0.05 => float freqInterpolation;  // in secondi

[21,22,23,24,25,26,27,28] @=> int knobs[];  // control change corrispondenti agli 8 potenziometri presenti sul controller

0.2 => amp;
osc.gain(amp); // impostare l'ampiezza di "osc" su "amp", cioe' 0.2
10::ms => dur attackTime;  // tempo di attacco
500::ms => dur releaseTime; // tempo di rilascio

line.time(freqInterpolation); // imposta il tempo di interpolatione della frequenza dell'oscillatore

// loop infinito
while(true)
{
  //  avanza ad ogni messaggio MIDI ricevuto

  mIn => now;

  while(mIn.recv(msgIn))
  {
    if(msgIn.data1 == (NOTE_ON + MIDI_CHANNEL) && msgIn.data3 != 0)
    {
      msgIn.data2 => midiNote;
      Std.mtof(midiNote) => fr;   // conversione da MIDI a Freq in Hz
      line.target(fr);  // assegnare la frequenza a 'line' che controlla la freq dell'oscillatore
      msgIn.data2 => lastNote;

      env.duration(attackTime);
      env.keyOn(1);

      <<< "KEY PRESSED" >>>;
    }

    // controlla che il messaggio MIDI sia un NOTE OFF o un NOTE ON con velocity 0
    // in entrambi i casi la nota MIDI deve combaciare con l'ultima suonata (playedNote)
    else if( (msgIn.data1 == (NOTE_ON + MIDI_CHANNEL) && msgIn.data3 == 0 && msgIn.data2 == lastNote) || (msgIn.data1 == (NOTE_OFF + MIDI_CHANNEL) && msgIn.data2 == lastNote) )
    {
      // impostare il tempo impegato dall'inviluppo per
      // passare da 1 a 0 (da massimo a minimo)
      env.duration(releaseTime);

      // triggerare l'inviluppo per farlo passare da ON a OFF
      env.keyOff(1);

      <<< "KEY RELEASED" >>>;
    }

    //  se il messaggio MIDI e' un Control Change ed il suo numero e' 21
    else if(msgIn.data1 == (CTRL_CHANGE+MIDI_CHANNEL) && msgIn.data2 == knobs[0])
    {
      setIntep(msgIn.data3) => freqInterpolation;
      line.time(freqInterpolation);
    }
  }
}

// -------------FUNZIONI--------------
function float setIntep(float x)
{
  x / 127. => x;  //  scala in un range da 0 a 1
  x*x*x => x; //  rendi la funzione esponenziale
  (x * 0.997) + 0.003 => x; //  scala in un range da 3 a 1000 millisecondi

  <<< "Freq Interpolation: " + x + " ms" >>>;

  return x;
}
