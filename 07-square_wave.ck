/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo.
"Replace Shred" serve a far ripartire il programma.
----------------------------------------------------*/

// RTcMix Lookup table: https://en.wikipedia.org/wiki/Real-time_Cmix
Gen10 square_1; // note da 0 a 60
Gen10 square_2; // note da 61 a 80
Gen10 square_3; // note da 81 a 105
Gen10 square_4; // note da 106 a 127
Phasor phasor;  // utilizzato per controllare gli UGen Gen10

Step step => Envelope line => phasor;
Envelope env => Gain master => dac;

step.next(1); // imposta il valore di 'step' su 1
phasor.sync(0);

initSquareWave(); // inizializza gli UGen Gen10

// connessioni MIDI
MidiIn mIn; // crea un input MIDI
MidiMsg msgIn;  // crea un contenitore per i messaggi MIDI ricevuti

// selezionare la porta MIDI che corrisponde al
// controller in uso (CTRL+2 per controllare le porte MIDI)
mIn.open( "Launchkey MK2 25 MIDI 1" );

// variabili
144 => int NOTE_ON;
128 => int NOTE_OFF;
176 => int CTRL_CHANGE;
0 => int MIDI_CHANNEL;  // canale MIDI - range da 0 a 15

float fr;
float amp;
int midiNote;
int lastNote;
0.007874016 => float dividedBy127;
0.05 => float freqInterpolation;  // in secondi

[21,22,23,24,25,26,27,28] @=> int knobs[];  // control change corrispondenti agli 8 potenziometri presenti sul controller

0.5 => amp;
master.gain( amp );                 // impostare l'ampiezza del Gain 'master'
10::ms => dur attackTime;           // tempo di attacco
500::ms => dur releaseTime;         // tempo di rilascio

line.time( freqInterpolation );     // imposta il tempo di interpolatione della frequenza dell'oscillatore

// loop infinito
while( true )
{
  // avanza ad ogni messaggio MIDI ricevuto
  mIn => now;

  while( mIn.recv( msgIn ) )
  {
    if( msgIn.data1 == ( NOTE_ON + MIDI_CHANNEL ) && msgIn.data3 != 0 )
    {
      // resetta il volume prima di generare una nuova nota per evitare 'glitch'
      env.duration( 3::ms );
      env.keyOff(1);
      3::ms => now;

      msgIn.data2 => midiNote;
      setSquareWave( midiNote );    // seleziona la wavetable in base alla nota ricevuta
      Std.mtof( midiNote ) => fr;   // conversione da MIDI a Freq in Hz
      line.target( fr );            // assegnare la frequenza a 'line' che controlla la freq dell'oscillatore
      msgIn.data2 => lastNote;

      msgIn.data3 * dividedBy127 => float amplitude;
      env.gain( amplitude );

      env.duration( attackTime );
      env.keyOn(1);
    }

    // controlla che il messaggio MIDI sia un NOTE OFF o un NOTE ON con velocity 0
    // in entrambi i casi la nota MIDI deve combaciare con l'ultima suonata (playedNote)
    else if( ( msgIn.data1 == ( NOTE_ON + MIDI_CHANNEL ) && msgIn.data3 == 0 && msgIn.data2 == lastNote )
              || ( msgIn.data1 == ( NOTE_OFF + MIDI_CHANNEL ) && msgIn.data2 == lastNote ) )
    {
      env.duration( releaseTime );
      env.keyOff(1);
    }

    // portamento
    else if( msgIn.data1 == ( CTRL_CHANGE+MIDI_CHANNEL ) && msgIn.data2 == knobs[0] )
    {
      setIntep( msgIn.data3 ) => freqInterpolation;
      line.time( freqInterpolation );
    }
  }
}

// -------------FUNZIONI--------------
function float setIntep( float x )
{
  x * dividedBy127 => x;        // scala in un range da 0 a 1
  x * x * x => x;               // rendi la funzione esponenziale
  ( x * 0.997 ) + 0.003 => x;   // scala in un range da 3 a 1000 millisecondi

  <<< "Freq Interpolation: " + x + " sec" >>>;

  return x;
}

function void setSquareWave( int x )
{
  // seleziona l'UGen Gen10 in base alla nota ricevuta.
  // I diversi Gen10 hanno un diverso numero di armoniche,
  // piu' e' alta la nota selezionata meno armoniche si avranno
  // per evitare problemi di aliasing
  if( x <= 70 && square_1.isConnectedTo( env ) == 0 )
  {
    phasor => square_1 => env;
    phasor =< square_2 =< env;
    phasor =< square_3 =< env;
    phasor =< square_4 =< env;

    <<< "SQUARE 1" >>>;
  }
  else if(  x > 70 && x <= 90 && square_2.isConnectedTo( env ) == 0 )
  {
    phasor =< square_1 =< env;
    phasor => square_2 => env;
    phasor =< square_3 =< env;
    phasor =< square_4 =< env;

    <<< "SQUARE 2" >>>;
  }
  else if( x > 90 && x <= 108 && square_3.isConnectedTo( env ) == 0 )
  {
    phasor =< square_1 =< env;
    phasor =< square_2 =< env;
    phasor => square_3 => env;
    phasor =< square_4 =< env;

    <<< "SQUARE 3" >>>;
  }
  else if( x > 108 && x <= 127 && square_4.isConnectedTo( env ) == 0 )
  {
    phasor =< square_1 =< env;
    phasor =< square_2 =< env;
    phasor =< square_3 =< env;
    phasor => square_4 => env;

    <<< "SQUARE 4" >>>;
  }
}

function void initSquareWave()
{
  // crea 4 diverse lookup table, con contenuti armonici different.
  // onda quadra - formula: y = sum[(1/k)*sin(2PI*f*k*t)]; con k=1,3,5,7,9,...

  float coefficients[0];  // coefficienti degli UGen Gen10

  // square_1: 14 parziali
  for( 1=>int c; c<15; c++ )
  {
      ( ( c * 2 ) - 1 ) => int odd; // solo numeri dispari
      coefficients.size( odd ); // ridimensiona l'array
      1. / odd => float amp;  // 1/n
      amp => coefficients[ odd-1 ];
  }
  square_1.coefs( coefficients );

  // square_2: 7 parziali
  for( 1=>int c; c<8; c++ )
  {
    ( ( c * 2 ) - 1 ) => int odd;
    coefficients.size( odd );
    1. / odd => float amp;
    amp => coefficients[ odd-1 ];
  }
  square_2.coefs( coefficients );

  // square_3: 3 parziali
  for( 1=>int c; c<4; c++ )
  {
    ( ( c * 2 ) - 1 ) => int odd;
    coefficients.size( odd );
    1. / odd => float amp;
    amp => coefficients[ odd-1 ];
  }
  square_3.coefs( coefficients );

  // square_4: 1 parziale - sinusoide
  for( 1=>int c; c<2; c++ )
  {
    ( ( c * 2 ) - 1 ) => int odd;
    coefficients.size( odd );
    1. / odd => float amp;
    amp => coefficients[ odd-1 ];
  }
  square_4.coefs( coefficients );

}
// _FUNZIONI end
