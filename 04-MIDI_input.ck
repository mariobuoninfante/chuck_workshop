/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo.
"Replace Shred" serve a far ripartire il programma.
----------------------------------------------------*/

//oscillatore sinusoidale collegato ad un inviluppo
SinOsc sine => Envelope env => dac;

//connessioni MIDI
MidiIn mIn; // crea un input MIDI
MidiMsg msgIn;  // crea un contenitore per i messaggi MIDI ricevuti

/*
connettere la porta MIDI.
Utilizzare "CTRL+2" per visualizzare le device MIDI connesse al computer,
o cliccare su "Window->Device Browser".
Si puo' utilizzare sia il numero sia il nome della porta. 
Nel caso del numero le virgolette "" non sono necessarie. 
Nel caso del nome si, in quanto l'argomento e' di tipo "string".
*/
mIn.open("Virtual Keyboard");   //sostituire "Virtual Keyboard" con il corretto nome della device MIDI connessa

// variabili
float fr;
float amp;
int midiNote;

0.2 => amp;
sine.gain(amp); // impostare l'ampiezza di "sine" su "amp", cioe' 0.2
10::ms => dur attackTime;  // tempo di attacco
500::ms => dur releaseTime; // tempo di rilascio

// cambiando i valori assegnati ad "attackTime" e "releaseTime" 
// si influenza il comportamento dell'inviluppo

// loop infinito
while(true)
{   
    // quando un (qualsiasi) messaggio MIDI viene ricevuto 
    // avanza nel programma
    mIn => now; 
    
    // altro loop (non infinito) dove passare i messaggi MIDI
    // all'oscillatore e l'inviluppo quando la porta "mIn" riceve (recv) 
    // un messaggio (msgIn)
    // esegui le operazioni tra parentesi graffe {}
    while(mIn.recv(msgIn))  
    {
        // prendere il MIDI byte 2 (nel caso di messaggi NOTE ON/OFF e' il pitch)
        // ed assegnarlo alla variabile di tipo int "midiNote"
        msgIn.data2 => midiNote; 
        Std.mtof(midiNote) => fr;   // conversione da MIDI a Freq in Hz
        sine.freq(fr);  // assegnare la frequenza all'oscillatore
        
        // se (if) il MIDI byte 3 (nel caso di NOTE ON/OFF e' la velocity)
        // e' diverso da zero (quindi la velocity e' diversa da zero)
        // eseguire il codice tra le parentesi graffe {}
        if(msgIn.data3 != 0)
        {
            // impostare il tempo impegato dall'inviluppo per
            // passare da 0 a 1 (da minimo a massimo)
            env.duration(attackTime);
            // triggerare l'inviluppo per farlo passare da OFF a ON
            env.keyOn(1);
            
            <<< "KEY PRESSED" >>>;
        }
        
        // "else" (altrimenti) e' riferito al costrutto precedente ("if").
        // Nel caso in cui msgIn.data3 (velocity nel caso di NOTE ON/OFF) e' uguale a 0 
        // (dunque il codice contenuto nelle parentesi graffe {} dopo "if" non e' stato eseguito)
        // eseguire il codice contenuto nelle parentesi graffe {} dopo "else"
        else
        {
            // impostare il tempo impegato dall'inviluppo per
            // passare da 1 a 0 (da massimo a minimo)
            env.duration(releaseTime);
            
            // triggerare l'inviluppo per farlo passare da ON a OFF
            env.keyOff(1);
            
            <<< "KEY RELEASED" >>>;           
        }          
    }
}