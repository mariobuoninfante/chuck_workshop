/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo.
"Replace Shred" serve a far ripartire il programma.
----------------------------------------------------*/

//oscillatore sinusoidale collegato ad un inviluppo
SinOsc sine => Envelope env => dac;

//connessioni MIDI
MidiIn mIn; // crea un input MIDI
MidiMsg msgIn;  // crea un contenitore per i messaggi MIDI ricevuti

/*
connettere la porta MIDI.
Utilizzare "CTRL+2" per visualizzare le device MIDI connesse al computer,
o cliccare su "Window->Device Browser".
Si puo' utilizzare sia il numero sia il nome della porta. 
Nel caso del numero le virgolette "" non sono necessarie. 
Nel caso del nome si, in quanto l'argomento e' di tipo "string".
*/
mIn.open(1);   // selezionare la porta MIDI che corrisponde al controller utilizzato (CTRL+2 per controllare le porte MIDI) 

// variabili
144 => int NOTE_ON;
128 => int NOTE_OFF;
0 => int MIDI_CHANNEL;// MIDI channel da 0 a 15 

float fr;
float amp;
int midiNote;
int lastNote;

0.2 => amp;
sine.gain(amp); // impostare l'ampiezza di "sine" su "amp", cioe' 0.2
10::ms => dur attackTime;  // tempo di attacco
500::ms => dur releaseTime; // tempo di rilascio

// cambiando i valori assegnati ad "attackTime" e "releaseTime" 
// si influenza il comportamento dell'inviluppo

// loop infinito
while(true)
{   
    // quando un (qualsiasi) messaggio MIDI viene ricevuto 
    // avanza nel programma
    mIn => now; 
    
    // altro loop (non infinito) dove passare i messaggi MIDI
    // all'oscillatore e l'inviluppo quando la porta "mIn" riceve (recv) 
    // un messaggio (msgIn)
    // esegui le operazioni tra parentesi graffe {}
    while(mIn.recv(msgIn))  
    {
        // stampa sulla console il messaggio MIDI raw
        <<< "Raw MIDI msg: " + msgIn.data1, msgIn.data2, msgIn.data3 >>>;
        
        // controlla che il messaggio MIDI sia un NOTE ON con velocity maggiore di 0
        if(msgIn.data1 == (NOTE_ON + MIDI_CHANNEL) && msgIn.data3 != 0)
        {
            // prendere il MIDI byte 2 (nel caso di messaggi NOTE ON/OFF e' il pitch)
            // ed assegnarlo alla variabile di tipo int "midiNote"
            msgIn.data2 => midiNote; 
            Std.mtof(midiNote) => fr;   // conversione da MIDI a Freq in Hz
            sine.freq(fr);  // assegnare la frequenza all'oscillatore
            msgIn.data2 => lastNote;
            // impostare il tempo impegato dall'inviluppo per
            // passare da 0 a 1 (da minimo a massimo)
            env.duration(attackTime);
            // triggerare l'inviluppo per farlo passare da OFF a ON
            env.keyOn(1);
            
            <<< "KEY PRESSED" >>>;
        }
        
        // controlla che il messaggio MIDI sia un NOTE OFF o un NOTE ON con velocity 0
        // in entrambi i casi la nota MIDI deve combaciare con l'ultima suonata (playedNote)
        else if( (msgIn.data1 == (NOTE_ON + MIDI_CHANNEL) && msgIn.data3 == 0 && msgIn.data2 == lastNote) || (msgIn.data1 == (NOTE_OFF + MIDI_CHANNEL) && msgIn.data2 == lastNote) )
        {
            // impostare il tempo impegato dall'inviluppo per
            // passare da 1 a 0 (da massimo a minimo)
            env.duration(releaseTime);
            
            // triggerare l'inviluppo per farlo passare da ON a OFF
            env.keyOff(1);
            
            <<< "KEY RELEASED" >>>;           
        }          
    }
}