/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo
"Replace Shred" serve a far ripartire il programma.
----------------------------------------------------*/

// oscillatore sinusoidale collegato ad un inviluppo
SinOsc sine => Envelope env => dac;

// variabili
float fr;
float amp;
int midiNote;
dur t; 

// assegnare valori alle variabili
0.2 => amp;
sine.gain(amp); // impostare l'ampiezza di "sine" su "amp", cioe' 0.2
100::ms => dur attackTime;  // tempo di attacco
990::ms => dur releaseTime; // tempo di rilascio

// cambiando i valori assegnati ad "attackTime" e "releaseTime" 
// si influenza il comportamento dell'inviluppo

// loop infinito
while(true)
{
    Math.random2(48, 72) => midiNote;   // nota MIDI random
    Std.mtof(midiNote) => fr;   // conversione da MIDI a Freq in Hz
    sine.freq(fr);  // assegnare la frequenza all'oscillatore
    
    // impostare il tempo impegato dall'inviluppo per
    // passare da 0 a 1 (da minimo a massimo)
    env.duration(attackTime);
    // triggerare l'inviluppo per farlo passare da OFF a ON
    env.keyOn(1);
    
    <<< "KEY PRESSED" >>>;
    
    // aspettare a finche' l'inviluppo raggiunge il valore massimo (1)
    attackTime => now;
    
    // impostare il tempo impegato dall'inviluppo per
    // passare da 1 a 0 (da massimo a minimo)
    env.duration(releaseTime);
    
    // triggerare l'inviluppo per farlo passare da ON a OFF
    env.keyOff(1);
    
    <<< "KEY RELEASED" >>>;
    
    // aspettare a finche' l'inviluppo raggiunge il valore minimo (0), cioe' il silenzio
    releaseTime => now;
}