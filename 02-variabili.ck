/*-----------------------INFO-------------------------
PREMI "Add Shred" per lanciare il programma
e "Remove Shred" per terminarlo
----------------------------------------------------*/

//oscillatore sinusoidale
SinOsc sine => dac;

//variabili
float fr; //variabile di tipo float (numero in virgola mobile)
float amp;
int midiNote;   //variabile di tipo int (numero intero)
dur t;  //variabile di tipo dur (durata in ChucK - puo' essere week, day, hour, minute, second, ms, sample)

//assegnare valori alle variabili ("valore => nome della variabile")
0.2 => amp;
500::ms => t;   //500 millisecondi (lo stesso di 0.5::second)

//loop infinito
//all'inizio di ogni loop una frequeza random viene assegnata
//all'oscillatore sinusoidale "sine"
while(true)
{
    //assegnare un numero intero random (nel range da 48 a 72) 
    //alla variabile "midiNote". Questo numero rappresenta la nota MIDI
    Math.random2(48, 72) => midiNote;
    
    //convertire la nota MIDI in frequenza in Hertz e assegnarla alla variabile "fr"
    //"mtof" e' l'acronimo di MIDI to Frequency
    Std.mtof(midiNote) => fr;
    
    //assegnare "fr" all'oscillatore "sine"
    sine.freq(fr);
    sine.gain(amp);
    
    //stampa sulla console (CTRL+0) la frequenza in Hz assegnata all'oscillatore
    <<< "MIDI NOTE: " + midiNote + " || " + "FREQ Hz: " + fr + " Hz" >>>;
    
    t => now;
}

/*
"Math" e "Std" (Standard) sono due librerie di ChucK.
La prima contiente funzioni matematiche, la seconda puo' essere considerata
come una libreria di "utilities".
Una specifica funzione di una libreria puo' essere utilizzata
specificando il nome della libreria seguita da "." e il nome della funzione
ie: Math.sin(x)
dove "Math" e' il nome della libreria, mentre "sin" il metodo, la funzione parte della
libreria. "x" e' l'argomento. Per "argomento" si intende un valore passato
alla funzione. Ogni metodo puo' avere bisogno da 0 a n argomenti.
Infatti Math.random2(x,y) ha bisogno di 2 argomenti che specificano il range 
del numero random generato.
*/

/*
la frase "MIDI NOTE: " che e' stampata sulla console alla fine di ogni loop
e' un dato di tipo "string", una stringa di testo composta da 0 a n parole.
I dati "string" sono sempre contenuti tra virgolette "".
ie: 
string "metronome";
oppure: 
string "ChucK Programming Language";
*/